package com.app.services;

import com.app.entity.User;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface UserService {

 boolean saveFromFile(MultipartFile file) ;

    List<User> findAll();
}
