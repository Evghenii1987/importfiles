package com.app.services.impl;

import com.app.entity.User;
import com.app.repository.UserRepo;
import com.app.services.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepo userRepo;

    @Override
    public boolean saveFromFile(MultipartFile file) {
        boolean flag = false;
        String extension = FilenameUtils.getExtension(file.getOriginalFilename());

        if (extension.equalsIgnoreCase("json")) {
            flag = readDataFromJSON(file);
        } else if (extension.equalsIgnoreCase("csv")) {
            flag = readDataFromCSV(file);
        } else if (extension.equalsIgnoreCase("xlsx")) {
            flag = readDataFromExcel(file);
        }
        return flag;
    }


    private boolean readDataFromExcel(MultipartFile file) {
        Workbook workbook = getWorkbook(file);
        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rows = sheet.iterator();

        rows.next();
        while (rows.hasNext()) {
            Row row = rows.next();
            User user = new User();

            if (row.getCell(0).getCellType() == CellType.STRING) {
                user.setName(row.getCell(0).getStringCellValue());
            }
            if (row.getCell(1).getCellType() == CellType.STRING) {
                user.setSurname(row.getCell(1).getStringCellValue());
            }
            if (row.getCell(2).getCellType() == CellType.STRING) {
                user.setEmail(row.getCell(2).getStringCellValue());
            }
            if (row.getCell(3).getCellType() == CellType.STRING) {
                user.setGender(row.getCell(3).getStringCellValue());
            }
            if (row.getCell(4).getCellType() == CellType.STRING) {
                user.setImg(row.getCell(4).getStringCellValue());
            }
            if (row.getCell(5).getCellType() == CellType.STRING) {
                user.setPaymentMethods(row.getCell(5).getStringCellValue());
            }
            if (row.getCell(6).getCellType() == CellType.NUMERIC ) {
                user.setAmount(row.getCell(6).getNumericCellValue());
            }
            if (row.getCell(7).getCellType() == CellType.BOOLEAN) {
                user.setConfirmation(row.getCell(7).getBooleanCellValue());
            }
            if (row.getCell(8).getCellType() == CellType.BOOLEAN) {
                user.setMoneyTransferred(row.getCell(8).getBooleanCellValue());
            }
            if (row.getCell(9).getCellType() == CellType.STRING) {
                user.setCity(row.getCell(9).getStringCellValue());
            }

        user.setFileType(FilenameUtils.getExtension(file.getOriginalFilename()));
            userRepo.save(user);
        }
        return true;
    }

    private Workbook getWorkbook(MultipartFile file) {
        Workbook workbook = null;
        String extension = FilenameUtils.getExtension(file.getOriginalFilename());
        try {
            if (extension.equalsIgnoreCase("xlsx")) {
                workbook = new XSSFWorkbook(file.getInputStream());
            } else if (extension.equalsIgnoreCase("xls")) {
                workbook = new HSSFWorkbook(file.getInputStream());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return workbook;
    }


    private boolean readDataFromCSV(MultipartFile file) {

        try {
            InputStreamReader reader = new InputStreamReader(file.getInputStream());
            CSVReader csvReader = new CSVReaderBuilder(reader).withSkipLines(1).build();
            List<String[]> rows = csvReader.readAll();
            for (String[] row : rows
            ) {
                userRepo.save(new User(row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8], row[9]));
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private boolean readDataFromJSON(MultipartFile file) {
        try {
            InputStream inputStream = file.getInputStream();
            ObjectMapper mapper = new ObjectMapper();

            List<User> users = Arrays.asList(mapper.readValue(inputStream, User[].class));

            if (users != null && users.size() > 0) {
                for (User user : users) {
                    user.setFileType(FilenameUtils.getExtension(file.getOriginalFilename()));
                    userRepo.save(user);


                }
            }
            return true;
        } catch (Exception e) {
            return false;
        }

    }

    @Override
    public List<User> findAll() {
        return (List<User>) userRepo.findAll();
    }
}



