package com.app.controller;

import com.app.entity.User;
import com.app.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
public class MainController {
    @Autowired
    private UserService userService;

    @GetMapping("/list")
    public String main(Model model){
        model.addAttribute("user", new User());
       List<User> users= userService.findAll();
        model.addAttribute("users", users);
        return "main";
    }

    @PostMapping ("/fileupload")
    public String uploadFile(@ModelAttribute User user, RedirectAttributes redirectAttributes){
          boolean flag= userService.saveFromFile(user.getFile());
         if (flag){
             redirectAttributes.addFlashAttribute("success", "Success, file is upload.");
         }else {
             redirectAttributes.addFlashAttribute("error", "Some Error happened,please try again.");
         }
        return "main";
    }
}
