package com.app.entity;

import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;

@Entity
@Table(name = "usr")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "A")
    private String name;

    @Column(name = "B")
    private String surname;

    @Column(name = "C")
    private String email;

    @Column(name = "D")
    private String gender;

    @Column( name = "E")
    private String img;

    @Column(name = "F")
    private String paymentMethods;

    @Column(name = "G")
    private Double amount;

    @Column(name = "H")
    private Boolean confirmation;

    @Column(name = "I")
    private Boolean moneyTransferred;

    @Column(name = "J")
    private String city;
    @Transient
    private String fileType;


    @Transient
    private MultipartFile file;


    public User() {
    }

    public User(String name, String surname, String email,
                String gender, String img, String paymentMethods, Double amount, Boolean confirmation,
                Boolean moneyTransferred, String city) {
        this.name=name;
        this.surname=surname;
        this.email=email;
        this.gender=gender;
        this.img=img;
        this.paymentMethods=paymentMethods;
        this.amount=amount;
        this.confirmation=confirmation;
        this.moneyTransferred=moneyTransferred;
        this.city=city;
    }

    public User(String name, String surname, String email, String gender, String img, String paymentMethods, String s, String confirmation, String moneyTransferred, String city) {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getPaymentMethods() {
        return paymentMethods;
    }

    public void setPaymentMethods(String paymentMethods) {
        this.paymentMethods = paymentMethods;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Boolean isConfirmation() {
        return confirmation;
    }

    public void setConfirmation(Boolean confirmation) {
        this.confirmation = confirmation;
    }

    public Boolean isMoneyTransferred() {
        return moneyTransferred;
    }

    public void setMoneyTransferred(Boolean moneyTransferred) {
        this.moneyTransferred = moneyTransferred;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", email='" + email + '\'' +
                ", gender='" + gender + '\'' +
                ", img='" + img + '\'' +
                ", paymentMethods='" + paymentMethods + '\'' +
                ", amount=" + amount +
                ", confirmation=" + confirmation +
                ", moneyTransferred=" + moneyTransferred +
                ", city='" + city + '\'' +
                '}';
    }
}
