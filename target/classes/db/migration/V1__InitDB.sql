create table hibernate_sequence (
    next_val bigint
                                );
insert into hibernate_sequence
values ( 1 );

create table usr (
        id bigint not null,
        a varchar(255),
        b varchar(255),
        c varchar(255),
        d varchar(255),
        e TEXT,
        f varchar(255),
        g double,
        h boolean,
        i boolean,
        j varchar(255),
        primary key (id)
                 );
